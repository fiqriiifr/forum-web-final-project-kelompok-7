@extends('layouts.detail-master')

@section('title')
    Detail Pertanyaan
@endsection

@section('content')
<div class="card">
    <div class="card-body">
        <h5 class="keterangan-card">Question</h5>
        <div class="container-fluid mt-4">
            <div class="row">
                <div class="col-1 p-0 d-flex justify-content-center">
                    <img src="{{asset('images/1662306577.jpg')}}" alt="gambar_keterangan_pertanyaan" class="img-user">
                </div>
        
                <div class="col-10 p-0">
                    <h4>Kadek Frama</h4>
                    <p class="created_at">{{$pertanyaan->created_at}}</p>
                </div>
        
                <div class="col-1">
                    <button class="btn btn-primary btn-sm" disabled>test</button>
                </div>
            </div>
        
            <div class="row px-3">
                <h3 class="col my-4">{{$pertanyaan->content_pertanyaan}}</h3>
            </div>
        
            <div class="row d-flex justify-content-center">
                <img src="{{asset('images/'.$pertanyaan->gambar)}}" alt="keterangan_gambar_pertanyaan" class="img-gambar-pertanyaan">
            </div>
        
            <div class="row mt-5 px-3">
                <form action="#" class="col">
                    <input type="text" class="form-control" placeholder="Add Answer...">
                    <button type="button" class="btn btn-outline-success btn-fw my-3">Add</button>
                    <a href="/pertanyaan" class="btn btn-outline-secondary btn-fw my-3 ml-2">Back</a>
                </form>
            </div>
        </div>
    </div>
</div>


<div class="card mt-3">
    <div class="card-body">
        <h5 class="keterangan-card">Answer</h5>
        <div class="container-fluid mt-4">
            <div class="row">
                <div class="col-1 p-0 d-flex justify-content-center">
                    <img src="{{asset('images/1662306577.jpg')}}" alt="gambar_keterangan_pertanyaan" class="answer-img">
                </div>
        
                <div class="col-10 p-0">
                    <h5>Kadek Frama</h5>
                    <p class="created_at">{{$pertanyaan->created_at}}</p>
                </div>
            </div>
        
            <div class="row px-3">
                <p class="col my-4 answer-text">Apakah tips yang sebaiknya dilakukan untuk programmer frontend pemula?Apakah tips yang sebaiknya dilakukan untuk programmer frontend pemula?Apakah tips yang sebaiknya dilakukan untuk programmer frontend pemula?</p>
            </div>
        </div>
    </div>
</div>



@endsection
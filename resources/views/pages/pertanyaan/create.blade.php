@extends('layouts.master')

@section('title')
    Create Pertanyaan
@endsection

@section('content')
    <form action="/pertanyaan" method="POST" class="forms-sample" enctype="multipart/form-data">
        @csrf
        <div class="form-group">
            <label for="exampleTextarea1">Pertanyaan</label>
            <textarea name="pertanyaan" class="form-control" rows="4" placeholder="Ada pertanyaan...?"></textarea>
          </div>
        @error('pertanyaan')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror

        
        <div class="form-group">
            <label for="exampleInputName1">Keterangan Gambar</label>
            <input type="file" name="gambar" class="form-control"">
        </div>
        @error('gambar')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror

        
        <div class="form-group">
            <label for="exampleInputName1">Kategori Pertanyaan</label>
            <select name="kategori_id" class="form-control">
                <option value="">--Pilih Kateogri--</option>
                @forelse ($kategori as $item)
                    <option value="{{$item->id}}">{{$item->nama}}</option>
                @empty
                    <option value="">Tidak ada Data Kteogri</option>
                @endforelse
            </select>
        </div>
        @error('kategori_id')
            <div class="alert alert-danger">{{$message}}</div>
        @enderror

        <button type="submit" class="btn btn-primary mr-2">Submit</button>
        <a href="/pertanyaan" class="btn btn-light">Cancel</a>
  </form>
@endsection
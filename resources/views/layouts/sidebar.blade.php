
            <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <ul class="nav">
                  <li class="nav-item">
                      <a class="nav-link" href="/dashboard">
                        <i class="icon-grid menu-icon"></i>
                        <span class="menu-title">Dashboard</span>
                      </a>
                  </li>
                  {{-- <li class="nav-item">
                      <a class="nav-link" href="/profile">
                        <i class="icon-head menu-icon"></i>
                        <span class="menu-title">Profile Saya</span>
                      </a>
                  </li> --}}
                  <li class="nav-item">
                      <a class="nav-link collapsed" data-toggle="collapse" href="#form-elements" aria-expanded="false" aria-controls="form-elements">
                        <i class="icon-columns menu-icon"></i>
                        <span class="menu-title">Forum Diskusi</span>
                        <i class="menu-arrow"></i>
                      </a>
                      <div class="collapse" id="form-elements" style="">
                        <ul class="nav flex-column sub-menu">
                          <li class="nav-item"><a class="nav-link" href="/pertanyaan">List Pertanyaan</a></li>
                          <li class="nav-item"><a class="nav-link" href="/pertanyaan/create">Buat Pertanyaaan</a></li>
                        </ul>
                      </div>
                  </li>
                  <li class="nav-item">
                      <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="false" aria-controls="ui-basic">
                        <i class="icon-layout menu-icon"></i>
                        <span class="menu-title">Kategori</span>
                        <i class="menu-arrow"></i>
                      </a>
                      <div class="collapse" id="ui-basic">
                        <ul class="nav flex-column sub-menu">
                          <li class="nav-item"> <a class="nav-link" href="/kategori">Kategori Topik</a></li>
                        </ul>
                      </div>
                    </li>
                </ul>
              </nav>